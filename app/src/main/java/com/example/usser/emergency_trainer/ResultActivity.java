package com.example.usser.emergency_trainer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        TextView resultTextView = findViewById(R.id.txt_finish_time);
        resultTextView.setText(AlarmActivity.getTimeReaction());
        ActionsLogWriter.setTextToWrite("Alarm reacted for " + AlarmActivity.getTimeReaction());
        TimerActivity.setAlarmExecutionCancel();
    }
}
