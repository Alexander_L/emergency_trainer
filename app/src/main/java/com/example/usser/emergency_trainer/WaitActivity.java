package com.example.usser.emergency_trainer;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Date;

public class WaitActivity extends AppCompatActivity {

    Date date;
    TextView time_to_train;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wait);
        time_to_train = findViewById(R.id.tV_time_to_train);
        time_to_train.setText(getTimeToTrain());
    }

    String getTimeToTrain () {
        return TimerActivity.getTimeToRun().toString();
    }

    public void onBtnAlarmCancelClick(View view) {
        TimerActivity.setAlarmExecutionCancel();
        time_to_train.setText(getResources().getString(R.string.alarm_canceled));
        Intent intent = new Intent(WaitActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void onShowLogBtnClick(View view) {
        Intent intent = new Intent(WaitActivity.this, TestingActivity.class);
        startActivity(intent);

    }
}
