package com.example.usser.emergency_trainer;


import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import android.content.Context;


public class ActionsLogWriter extends Application {

    private static final String FILE_NAME = "actions_log.txt";
    private static String textToWrite = "something";
    //private static FileOutputStream fos;
    static File file = new File(FILE_NAME);


    private static String getTextToWrite() {
        return textToWrite;
    }

     static void setTextToWrite(String textFromApp) {
        Calendar calendar = Calendar.getInstance();
        String logTime = calendar.getTime().toString();
        textToWrite = logTime + " " + textFromApp + "/n";
        writeFile(textToWrite);
    }

    private static void writeFile(String textToWrite) {
        if (!file.exists()) {
            try {
                file.createNewFile();
            }
            catch (Exception ex){
                System.out.println(ex);
            }
        }

        try {
            FileWriter fw = new FileWriter(file, true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(textToWrite);
            bw.flush();
            bw.close();
        }
        catch (IOException e)
        {
            System.out.println(e);
        }

        /*try {
            fos = openFileOutput(FILE_NAME,MODE_WORLD_READABLE);
            fos.write(getTextToWrite().getBytes());
            fos.flush();
            fos.close();
        }
        catch (Exception e) {
            System.out.println(e);
        }
*/
    }

}
