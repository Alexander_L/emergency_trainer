package com.example.usser.emergency_trainer;

import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

public class AlarmActivity extends AppCompatActivity {

    static long startEmergencyTime;
    static long finishEmergencyTime;
    SoundPool spAlarm;
    int sound_alarm = 1;
    boolean isAlarm = false; //колхозный триггер! Переделать!
    ConstraintLayout alarmLayout;
    TextView textViewHappendHere;
    Thread mAlarmColorsThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);
        TextView txt_happend_here = findViewById(R.id.txt_happend_here);
        txt_happend_here.setText(get_emergency_text());
        startEmergencyTime = System.currentTimeMillis();

        spAlarm = new SoundPool(1, AudioManager.STREAM_MUSIC,0);
        sound_alarm = spAlarm.load(this,R.raw.sound_alarm_w, 1);
        spAlarm.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                spAlarm.play(sound_alarm,1,1,0,0,1);
            }
        });
        isAlarm = true;
        setAlarmInfoColors();
        ActionsLogWriter.setTextToWrite("Alarm started.");
    }

    private String get_emergency_text () {
        return MainActivity.getEmergency_to_train();
    }

    public void onBtnRctClick(View view) {
        isAlarm = false;
        mAlarmColorsThread.interrupt();
        setFinishEmergencyTime();
        Intent intent = new Intent(AlarmActivity.this, ResultActivity.class);
        startActivity(intent);
        finish();
    }

    private void setFinishEmergencyTime () {
        finishEmergencyTime = System.currentTimeMillis();
    }

    private static long getReactTime () {
        return finishEmergencyTime - startEmergencyTime;
    }

    public static String getTimeReaction() {
        long reactTime = getReactTime();
        long seconds = TimeUnit.MILLISECONDS.toSeconds(reactTime);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(reactTime);
        long hours = TimeUnit.MILLISECONDS.toHours(reactTime);
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    private void setAlarmInfoColors() {
        alarmLayout = findViewById(R.id.constraintLayoutAlarm);
        textViewHappendHere = findViewById(R.id.txt_happend_here);
        mAlarmColorsThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (isAlarm) {
                        runOnUiThread(runnableRedWhite);
                        mAlarmColorsThread.sleep(1500);
                        runOnUiThread(runnableWhiteRed);
                        mAlarmColorsThread.sleep(1500);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });
        mAlarmColorsThread.start();
    }

    Runnable runnableRedWhite = new Runnable() {
        @Override
        public void run() {
            alarmLayout.setBackgroundColor(Color.RED);
            textViewHappendHere.setTextColor(Color.WHITE);
        }
    };

    Runnable runnableWhiteRed = new Runnable() {
        @Override
        public void run() {
            alarmLayout.setBackgroundColor(Color.WHITE);
            textViewHappendHere.setTextColor(Color.RED);
        }
    };
}