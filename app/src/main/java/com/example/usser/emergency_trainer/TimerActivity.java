package com.example.usser.emergency_trainer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class TimerActivity extends AppCompatActivity {

    private TimePicker mTimePicker;
    static int inputHour;
    static int inputMinute;
    static Timer executionTimer = new Timer();
    TimerTask alarmExecution;
    static Date timeToRun;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);
        mTimePicker = findViewById(R.id.timePicker);
        mTimePicker.setIs24HourView(true);
    }

    public void onBtnLaunchClick(View view) {
        setHoursMinutes();
        setTimeToRun();
        setAlarmExecution();
        setExecutionTimer();
        ActionsLogWriter.setTextToWrite("Launch planned at " + getTimeToRun().toString());
        Intent intent = new Intent(TimerActivity.this, WaitActivity.class);
        startActivity(intent);
        finish();
    }

    void setHoursMinutes () {
        inputHour = mTimePicker.getCurrentHour();
        inputMinute = mTimePicker.getCurrentMinute();
    }

    static void setTimeToRun () {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, inputHour);
        calendar.set(Calendar.MINUTE, inputMinute);
        timeToRun = calendar.getTime();
    }

    static Date getTimeToRun () {
        return timeToRun;
    }

    void setAlarmExecution () {
        alarmExecution = new TimerTask() {
            @Override
            public void run() {
                runAlarm();
            }
        };
    }

    void runAlarm () {
        Intent intent = new Intent(TimerActivity.this, AlarmActivity.class);
        startActivity(intent);
    }

    void setExecutionTimer () {
        executionTimer = new Timer();
        executionTimer.schedule(alarmExecution,timeToRun);
    }

    static void setAlarmExecutionCancel () {
        ActionsLogWriter.setTextToWrite("Alarm cancelled.");
        executionTimer.cancel();
    }

    public void onBtnBackClick(View view) {
        Intent intent = new Intent(TimerActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }


     /*   mTimePicker = findViewById(R.id.timePicker);
        mTimePicker.setIs24HourView(true);

    }

    public void onBtnLaunchClick(View view) {
        //setExecutionTimer();
        setAlarmExecution();
        Intent intent = new Intent(TimerActivity.this, TestingActivity.class);
        startActivity(intent);

    }

    void setExecutionTimer () {
        //executionTimer = new Timer();
        inputHour = mTimePicker.getCurrentHour();
        inputMinute = mTimePicker.getCurrentMinute();
        getCalendar();
        //executionTimer.schedule(alarmExecution,timeToRun);

    }

    void setAlarmExecution () {
        alarmExecution = new TimerTask() {
            @Override
            public void run() {
                runAlarm();
            }
        };
    }

    void runAlarm () {
        Intent intent = new Intent(TimerActivity.this, AlarmActivity.class);
        startActivity(intent);
    }

    *//*void setTimeToRun () { //Переделать с календарем.
        timeToRun = new Date();
        inputHour = Calendar.get(Calendar.HOUR_OF_DAY);
        timeToRun.setHours(inputHour);
        timeToRun.setMinutes(inputMinute);
    }*//*

    static Date getTimeToRun () {
        return timeToRun;
    }

    static void setAlarmExecutionCancel () {
        getCalendar();
        executionTimer.cancel();
    }

    static void getCalendar () {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, inputHour);
        calendar.set(Calendar.MINUTE, inputMinute);
        timeToRun = calendar.getTime();
    }*/
}