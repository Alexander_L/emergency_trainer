package com.example.usser.emergency_trainer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

public class TestingActivity extends AppCompatActivity {

    TextView textViewTest;
    static final String FILE_NAME = "actions_log.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testing);
        textViewTest = findViewById(R.id.textView_test);
        //textViewTest.setText(getTimeToRun());
        textViewTest.setText(readLogFile());

    }

    static String readLogFile () {
        String log = "";
        try {
            FileReader fr = new FileReader(FILE_NAME);
            BufferedReader br = new BufferedReader(fr);
            while ((br.readLine() != null)) {
                log = log.concat(log);
            }
        }
        catch (IOException e) {
            log = "File not found";
        }

        return log;
    }


}
    /*String getTimeToRun () {
        return TimerActivity.getTimeToRun().toString();
    }*/

