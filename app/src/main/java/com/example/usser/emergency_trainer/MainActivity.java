package com.example.usser.emergency_trainer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    ListView lvEmergencies;
    String [] emergencies;
    ArrayList selectedEmergencies;
    static String emergency_to_train = "something";
    Button btnNext;
    //Thread isSelectedListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvEmergencies = findViewById(R.id.emergencyList);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.emergencies,android.R.layout.simple_list_item_multiple_choice);
        lvEmergencies.setAdapter(adapter);
        emergencies = getResources().getStringArray(R.array.emergencies);

        btnNext = findViewById(R.id.btnNext);
        //setButtonNextText();

        lvEmergencies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //view.setSelected(true); Работает и без этого. Отвечает не за проверку выделения элементов списка. Сеттер же!
                /*if(lvEmergencies.isSelected()) {
                    btnNext.setText(R.string.select_launch_time); } Тоже не работает. Перегоняется на SparseBooleanArray
                else {btnNext.setText(R.string.select_position);}*/

                if (lvEmergencies.getCheckedItemCount() > 0) { //А вот это работает!!!
                    btnNext.setText(R.string.select_launch_time);
                }
                else {btnNext.setText(R.string.select_position);}
            }
        });
    }

    public void onNextBtnClick(View view) {
        selectedEmergencies = new ArrayList();
        SparseBooleanArray sbArray = lvEmergencies.getCheckedItemPositions();
        for (int i = 0; i < sbArray.size(); i++) {
            int key = sbArray.keyAt(i);
            if (sbArray.get(key)) {
                selectedEmergencies.add(emergencies[key]);
                select_emergency_to_train();
            }
        }
        ActionsLogWriter.setTextToWrite(setTextToLog());
        startActivity();
    }

    private int random_value () {
        Random rnd = new Random();
        return rnd.nextInt(selectedEmergencies.size());
    }

    private void select_emergency_to_train () {
        emergency_to_train = selectedEmergencies.get(random_value()).toString();
        // startActivity(); Так делать НЕЛЬЗЯ!!! Надо как-то переделать! Множатся Activity с TimePicker!!!!
    }

    public static String getEmergency_to_train() {
        return emergency_to_train;
    }

    private void startActivity () {
        Intent intent = new Intent(MainActivity.this, TimerActivity.class);
        startActivity(intent);
    }

    private String setTextToLog () {
        return "Selected " + getEmergency_to_train();
    }





//        isSelectedListView.interrupt();
    }

    /*private void setButtonNextText () {
        isSelectedListView = new Thread(new Runnable() {
            @Override
            public void run() {
                if (lvEmergencies.isSelected()) {
                    runOnUiThread(isSelectedItems);
                }
                else {
                    runOnUiThread(isNotSelectedItems);
                }
            }
        });
        isSelectedListView.start();
    }

    private Runnable isNotSelectedItems = new Runnable() {
        @Override
        public void run() {
            btnNext.setText(R.string.select_position);
        }
    };

    private Runnable isSelectedItems = new Runnable() {
        @Override
        public void run() {
            btnNext.setText(R.string.select_launch_time);
        }
    };*/

